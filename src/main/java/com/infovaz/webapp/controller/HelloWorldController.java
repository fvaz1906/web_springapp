package com.infovaz.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HelloWorldController
{
    @GetMapping("/hello/{name}")
    public @ResponseBody ModelAndView index(@PathVariable("name") String name) {
        ModelAndView mv = new ModelAndView("/hello");
        mv.addObject("title","SpringApp - Thymeleaf");
        mv.addObject("name", name);
        return mv;
    }

    @GetMapping("/hello")
    public @ResponseBody ModelAndView hello()
    {
        return new ModelAndView("/hello");
    }
}